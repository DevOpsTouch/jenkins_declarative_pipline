**What is Declarative Pipeline in Jenkins ?**

A declarative pipeline is a relatively new feature in Jenkins that provides a simpler and more intuitive way to define your pipeline stages and steps. In this type of pipeline, you define your pipeline as a series of stages, and each stage can have one or more steps.

**Here are the steps to set up a declarative pipeline in Jenkins:**

1. First, you need to have Jenkins installed and running. If you haven't installed Jenkins yet, you can follow the official installation instructions for your operating system.

2. Once Jenkins is up and running, create a new pipeline job. Click on "New Item" in the Jenkins dashboard, enter a name for your job, and select "Pipeline" as the job type.

3. In the pipeline configuration screen, scroll down to the "Pipeline" section and select "Pipeline script from SCM" as the definition. This will allow you to define your pipeline in a Jenkinsfile that you can store in your source code repository.

4. In the "SCM" section, select the type of source code management system that you are using (e.g. Git, SVN, etc.) and enter the repository URL and credentials if needed.

5. In the "Script Path" field, enter the path to the Jenkinsfile within your repository. If your Jenkinsfile is located at the root of your repository, you can just enter "Jenkinsfile".

6. Click on "Save" to save your pipeline job configuration.

7. Now, create a new Jenkinsfile in your source code repository at the location you specified in the "Script Path" field. The Jenkinsfile is where you define your pipeline stages and steps using the declarative syntax.


Here is an example Jenkinsfile that you can use as a starting point:

```
pipeline {
    agent any
    
    stages {
        stage('Build') {
            steps {
                sh 'echo "Building..."'
            }
        }
        stage('Test') {
            steps {
                sh 'echo "Testing..."'
            }
        }
        stage('Deploy') {
            steps {
                sh 'echo "Deploying..."'
            }
        }
    }
}
```

8. In this example, we define a pipeline with three stages: "Build", "Test", and "Deploy". Each stage has a single step that runs a shell command to echo a message. You can replace these shell commands with the actual build, test, and deployment commands for your project.

9. Commit and push your Jenkinsfile to your repository.

10. Now, you can trigger your pipeline job in Jenkins by clicking on "Build Now" or by configuring a trigger for your job (e.g. when changes are pushed to your repository).


**What is Scripted Pipeline in Jenkins**

A scripted pipeline is a Jenkins pipeline that is defined using Groovy script. It allows for more flexibility and complexity than declarative pipelines, but can also be more difficult to manage. Here are the steps to set up a scripted pipeline in Jenkins:

First, you need to have Jenkins installed and running. If you haven't installed Jenkins yet, you can follow the official installation instructions for your operating system.

1. Once Jenkins is up and running, create a new pipeline job. Click on "New Item" in the Jenkins dashboard, enter a name for your job, and select "Pipeline" as the job type.

2. In the pipeline configuration screen, scroll down to the "Pipeline" section and select "Pipeline script" as the definition. This will allow you to define your pipeline as a Groovy script.

3. In the "Script" field, enter the Groovy script that defines your pipeline stages and steps. Here is an example scripted pipeline that you can use as a starting point:


```
node {
    stage('Build') {
        sh 'echo "Building..."'
    }
    stage('Test') {
        sh 'echo "Testing..."'
    }
    stage('Deploy') {
        sh 'echo "Deploying..."'
    }
}
```

4. In this example, we define a pipeline with three stages: "Build", "Test", and "Deploy". Each stage has a single step that runs a shell command to echo a message. You can replace these shell commands with the actual build, test, and deployment commands for your project.

5. Click on "Save" to save your pipeline job configuration.
 
6. Now, you can trigger your pipeline job in Jenkins by clicking on "Build Now" or by configuring a trigger for your job (e.g. when changes are pushed to your repository).
 
7. That's it! You now have a basic scripted pipeline set up in Jenkins. You can customize your pipeline by adding more stages and steps, using Jenkins plugins, and defining more complex workflows.

8. It's worth noting that while scripted pipelines are more flexible, they can also be more complex to maintain and debug than declarative pipelines. If you're just getting started with Jenkins pipelines, it's often recommended to start with declarative pipelines and only move to scripted pipelines when necessary.



**Differnce between scripted pipeline and declearive pipeline**

Scripted and declarative pipelines are two different ways to define and implement Jenkins pipelines.

Scripted pipeline is written using Groovy scripting language and provides a lot of flexibility and power to create complex pipelines. With scripted pipeline, the pipeline is defined in the form of a script and executed on the Jenkins master node or on a separate agent. Scripted pipeline provides fine-grained control over the entire build process and allows for complex conditions, loops, and exception handling.

![](scrip.jpg)

Declarative pipeline, on the other hand, provides a more structured and concise way to write pipelines. With declarative pipeline, the pipeline is defined using a set of predefined steps, and it provides a simpler way to define pipelines, especially for new users. Declarative pipeline uses a more restricted syntax and enforces a set of best practices to ensure that the pipeline is easy to read and maintain.

The main difference between the two is the syntax and the level of control they provide. Scripted pipeline is more flexible but requires more expertise, while declarative pipeline is more structured and easier to read, but with less flexibility. It's important to choose the right pipeline syntax based on the specific requirements of your project.


Author: [Somay Mangla](https://www.linkedin.com/in/er-somay-mangla/) and [DevOps Touch](https://www.linkedin.com/company/devopstouch/)












